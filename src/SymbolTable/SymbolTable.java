
package SymbolTable;

import java.util.*;

/**
 * Class SymbolTable creates a hashtable that will store the identifiers recognized
 * by the parser. It includes the methods put() and get().
 * @author vpolowchak
 */
public class SymbolTable {
    private Hashtable table;
    
    /**
     * Constructor
     */
    public SymbolTable(){
        table = new Hashtable();
    }
    
    /**
     * Method put() stores an identifier in the symbol table. 
     * @param s This is the name of the identifier, and is the key value used to store the identifier in the hash table.
     * @param sym This Symbol object contains the kind [program, variable, array, function] of the identifier and
     * the type [integer, real] of the identifier if it is a variable. If it is not a variable, the type attribute will 
     * be an empty string.
     */
    public void put(String s, Symbol sym){
        table.put(s, sym);
    }
    
    /**
     * Method get() searches for a given key value in the hash table.
     * @param s This is the key value to search for.
     * @return The Symbol that matches the key value will be returned, if found. If the key value is not found, null
     * will be returned.
     */
    public Symbol get(String s){
        return (Symbol)table.get(s);        
    }
    
    /**
     * Method printTable calls the hashtable library function toString().
     * @return The string representation of the hashtable is returned.
     */
    public void printTable(){
        System.out.println("\n\n******************************************************************");
        System.out.println("************************** SYMBOL TABLE **************************");
        System.out.println();
        Enumeration en = table.keys(); // the list of all the hashtable elements
        String str; // the string to be returned to the calling method
        while(en.hasMoreElements()){
            String s1 = (String)en.nextElement(); // This will be the key in the hashtable, the name of the identifier
            Symbol sym = this.get(s1);  // Retrieve the symbol paired with the name of the identifier
            String s2 = sym.kind; // The kind of identifier
            if (sym.type == ""){
                String temp = "Name: " + s1 + "\t\tKind: " + s2 + "\t\tType: N/A";
                System.out.println(temp);
            }
            else {  // if sym.type is not null
                String s3 = sym.type; // The type of identifier: integer or real
                String temp = "Name: " + s1 + "\t\tKind: " + s2 + "\t\tType: " + s3;
                System.out.println(temp);
            }
        }
    }
} // end class SymbolTable()
