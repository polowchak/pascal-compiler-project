
package CompParser;

/**
 * The CompParser receives a valid token from the scanner, and then it processes the token
 * according to the grammar rules. The parser assures that the input file program will
 * follow the rules of the Pascal grammar that for which it was constructed.
 * @author Van Polowchak
 */

import CompScanner.CompScanner;
import PascalCompilerProject.NextTokenStatus;
import SymbolTable.Symbol;
import SymbolTable.SymbolTable;
import SyntaxTree.AssignmentStatementNode;
import SyntaxTree.CompoundStatementNode;
import SyntaxTree.DeclarationsNode;
import SyntaxTree.ExpressionNode;
import SyntaxTree.OperationNode;
import java.io.File;
import SyntaxTree.ProgramNode;
import SyntaxTree.StatementNode;
import SyntaxTree.SubprogramDeclarationsNode;
import SyntaxTree.ValueNode;
import SyntaxTree.VariableNode;
import java.util.ArrayList;



public class CompParser {
    
    private CompToken currentToken;
    private CompScanner scan;
    //private Env top = null; // holds addresses for symbol table levels
    //private int numOfIds = 5; // Number of identifiers that can be declared in one statement
    //private String[] identifiers = new String[numOfIds];  // will hold multiple identifiers of the same type, if present
    private SymbolTable symTab = new SymbolTable(); // will hold the program identifiers
    
    
    public CompParser(String filename) {
        File input = new File(filename);
        LookupTable symbols = new LookupTable();
        scan = new CompScanner(input, symbols);

        // Load in the first token as the lookahead token:
        scan.nextToken();
        currentToken = scan.getToken();
        //for (int i = 0; i < numOfIds; i++)
        //    identifiers[i] = null;   // initialize the identifiers array for use later in the parser
    } // end CompParser()
    
    
    /**
     * Parses a program.
     * Implements program --> program id; 
     *                        declarations    
     *                        subprogram_declarations  
     *                        compound_statement
     *                        .
     */
    public ProgramNode program() { 
        ProgramNode answer = null;
        try {
            System.out.println("in program()"); //**********************************************************************************************
            match(CompToken.PROGRAM);
            Symbol s = new Symbol("program", "");
            String programName = scan.getAttribute().toString();
            symTab.put(programName, s); // program identifier entered into symbol table
            answer = new ProgramNode(programName);
            match(CompToken.IDENTIFIER);
            match(CompToken.SEMICOLON);
            DeclarationsNode d = new DeclarationsNode();
            d = declarations();
            answer.setVariables(d);
            SubprogramDeclarationsNode sub = subprogram_declarations();
            answer.setFunctions(sub);
            CompoundStatementNode com = new CompoundStatementNode();
            com = compound_statement();
            answer.setMain(com);
            match(CompToken.PERIOD);
        }
        catch (Exception e) {
            error("Sorry, the program was unable to parse. The program() method failed.");                
        }
        return answer;
    } // end program()
    
    
    /**
     * Matches a token.
     * Matches the given token against the current token. Loads the next
     * token from the input file into the current token.
     * @param expectedToken The token to match. This parameter is a CompToken.
     */
    public void match(CompToken expectedToken) {
        System.out.println("in match()"); //***************************************************************************
              
        if(currentToken == expectedToken){
            NextTokenStatus tokenStatus = scan.nextToken();
            switch (tokenStatus) {
                case TOKEN_AVAILABLE:
                    currentToken = scan.getToken();
                    System.out.println("currentToken = " + currentToken); //*********************************************
                    break;
                case TOKEN_NOT_AVAILABLE: 
                    System.out.println("I'm right here - token not available");//***************************************************************
                    System.out.println("currentToken = " + currentToken.toString()); 
                    System.out.println("Current attribute = " + scan.getAttribute().toString());
                    error("Input error: Token not available.");
                    break;
                case INPUT_COMPLETE: // Add? ****************************************************************
                    break;
            }
        } else {
            error(expectedToken);  // We don't match!
        }
    } // end match(CompToken)
   
    
    
    
    /**
     * Parses declarations
     * Implements declarations -> var identifier_list : type ; declarations | lambda
     */
    public DeclarationsNode declarations() {
        System.out.println("in declarations()"); //*************************************************************************
        DeclarationsNode d = null;

        if ((currentToken == CompToken.VAR)) {
            //System.out.println("I'm right here");//************************************************************************
            //Env saved = top; // point to previous level in the symbol table ***************************************************************old symbol table code
            //top = new Env(top);  // create a new level in the symbol table *****************************************************************old symbol table code
            match(CompToken.VAR);

            ArrayList<String> identifiers = new ArrayList();
            identifiers = identifier_list(identifiers, 0); // Pass in the ArrayList and the index where we start inserting identifier names
            match(CompToken.COLON);
            String s = type(); // String s will be either "integer" or "real"

            if (!identifiers.isEmpty()) {
                d = new DeclarationsNode();
                int i = 0; // set index control
                while (i < identifiers.size()) {
                    VariableNode vn = new VariableNode(identifiers.get(i));
                    d.addVariable(vn);
                    Symbol sy = new Symbol("variable", s);
                    symTab.put(identifiers.get(i), sy); // Put the identifier, along with its kind and type, in the symbol table
                    i++;
                    /*   */
                }
            }
            match(CompToken.SEMICOLON);
            declarations();
        }
        // else lambda
        return d;
    } // end declarations()
     
    
    
     /**
     * Parses an identifier list.
     * Implements identifier_list -> id | id, identifier_list
     * @param idArray is an ArrayList that will hold the identifiers scanned from
     * the input program.
     * @param count is an integer used to keep track of the array index at which
     * we are inserting identifier names.
     * @return idArray will contain all of the identifier declarations and will be
     * returned to declarations().
     */
    public ArrayList<String> identifier_list(ArrayList<String> idArray, int count) {
        System.out.println("in identifier_list()"); //**********************************************************************8
        
        // Check the symbol table to make sure the incoming identifier is not already in use.
        Symbol sym1;
        sym1 = symTab.get(scan.getAttribute().toString());
        
        // If sym1 is null, the identifier was not found in the symbol table, and it is safe to insert it
        if (sym1 == null) {
            // count is a pointer to current empty index of identifiers array, in case of multiple identifiers of the same type.
            idArray.add(count, scan.getAttribute().toString());
            match(CompToken.IDENTIFIER);

            if (currentToken == CompToken.COMMA) {
                match(CompToken.COMMA);
                // If multiple identifiers of the same type are present, we need to store them in consecutive array indexes.
                identifier_list(idArray, ++count);
            }
        }
        
        // If sym1 is not null, then the incoming identifier is already in use and must not be used again
        else {
            error("Error in line number " + scan.getLineCount() + " of the input file. Identifier " + scan.getAttribute().toString() + " is already in use.");
        }
        return idArray;
    } // end identifier_list()
    
    
    
     /**
     * Parses a type
     * Implements type -> standard_type | array [num:num] of standard_type
     * @return str will either return null to declarations(), or it will return the value received 
     * from standard_type(), which will be either integer or real.
     */
    public String type() {
        System.out.println("in type()"); //******************************************************************************
        String str = null;
        if (currentToken == CompToken.ARRAY) {
            match(CompToken.ARRAY);
            match(CompToken.LEFT_SQ_BRACE);
            match(CompToken.NUMBER);
            match(CompToken.COLON);
            match(CompToken.NUMBER);
            match(CompToken.RIGHT_SQ_BRACE);           
            match(CompToken.OF);           
            standard_type("a");
        }
       
        else {
            str = standard_type("v");
        }
        return str;
    } // end type()
    
    
    
    /**
     * Parses a standard type Implements standard_type -> integer | real 
     * @param str: Accepts a parameter of type String which designates whether the incoming
     * identifier(s) are arrays or variables
     * @return s1: Returns the string value "integer" or "real" to type().
     */
    public String standard_type(String str) {
        System.out.println("in standard_type()");
        String s1 = null;
        if (currentToken == CompToken.REAL || currentToken == CompToken.INTEGER) {
            /* Not handling arrays right now ***************************************************************************************************************************
            if (str == "a") { // incoming identifier is an array
                int i = 0;
                while (identifiers[i] != null) { // The identifiers[] array may contain more than one array identifier
                    Symbol s = new Symbol("array", scan.getAttribute().toString());
                    symTab.put(identifiers[i], s); // Put the identifier, along with its kind and type, in the symbol table **************************symbol table code
                    i++;
                }
            }
            */
            if (str == "v") { // incoming identifier is a variable
               // int i = 0;
               // while (identifiers[i] != null) { // The identifiers[] array may contain more than one variable identifier
               //     Symbol s = new Symbol("variable", scan.getAttribute().toString());
               //     symTab.put(identifiers[i], s); // Put the identifier, along with its kind and type, in the symbol table ****************************symbol table code
               //     i++;
                s1 = scan.getAttribute().toString();
                }
            //}
            
            if (currentToken == CompToken.INTEGER){
                match(CompToken.INTEGER);
            }
            else {  // currentToken == REAL
                match(CompToken.REAL);
            }
            
            /* Not using the identifiers[] array any more
            // Reset the identifiers[] array
            for (int i = 0; i < numOfIds; i++) {
                identifiers[i] = null;
            }
            */
           
        } else {
            error("Error in standard_type(). Must be an integer or a real.");
        }
        return s1;
    } // end standard_type()
   
    
    
   /**
     * Parses subprogram declarations
     * Implements subprogram_declarations -> subprogram_declaration; subprogram_declarations | lambda
     */
    public SubprogramDeclarationsNode subprogram_declarations() {
        System.out.println("in subprogram_declarations"); //*************************************************************
        SubprogramDeclarationsNode answer = null;
        
        /* If the current Token is the keyword "function" or "procedure," we'll proceed with the method.
        Otherwise, there are no subprogram_declarations.
        */       
        if (currentToken == CompToken.FUNCTION ||
                currentToken == CompToken.PROCEDURE) {
            subprogram_declaration();
            match(CompToken.SEMICOLON);
            subprogram_declarations();
        }
        return answer;
    } // end subprogram_declarations()
    
    
    
     /**
     * Parses a subprogram declaration
     * <code>Implements subprogram_declaration -> subprogram_head
     *                                            declarations
     *                                            subprogram_declarations
     *                                            compound_statement
     * </code>
     */
    public void subprogram_declaration() {
        System.out.println("in subprogram_declaration()");
        subprogram_head();
        declarations();
        subprogram_declarations();
        compound_statement();
    } // end subprogram_declaration()
    
    
    
    
    /**
     * Parses a subprogram head.
     * Implements subprogram_head -> function id arguments : standard_type ; | procedure id arguments ;
     */
    public void subprogram_head() {
        System.out.println("in subprogram_head()");
        
        if (currentToken == CompToken.FUNCTION){       
            match(CompToken.FUNCTION);
            Symbol s = new Symbol("function", "");
            symTab.put(scan.getAttribute().toString(), s); // Put the identifier, along with its kind (function) and type (null), in the symbol table
            match(CompToken.IDENTIFIER);
            arguments();
            match(CompToken.COLON);
            standard_type("f"); // Send an "f" to satisfy the standard_type parameter requirement, but won't be used. Stands for "function."
            match(CompToken.SEMICOLON);
        }
        else if (currentToken == CompToken.PROCEDURE){       
            match(CompToken.PROCEDURE);
            Symbol s = new Symbol("procedure", "");
            symTab.put(scan.getAttribute().toString(), s); // Put the identifier, along with its kind (procedure) and type (null), in the symbol table
            match(CompToken.IDENTIFIER);
            arguments();
            match(CompToken.SEMICOLON);
        }
    } // end subprogram_head()
    
    /**
     * Parses a compound statement
     * Implements compound_statement -> begin optional_statements end
     */
    public CompoundStatementNode compound_statement() {
        System.out.println("in compound_statement"); //********************************************************************
        CompoundStatementNode answer = null;
        match(CompToken.BEGIN);
        
        // optional_statements -> statement_list -> statement -> variable -> identifier
        if (currentToken == CompToken.IDENTIFIER)
            answer = optional_statements();
        
        match(CompToken.END);
        return answer;
    } // end compound_statement()
    
    
   
   
    /**
     * Parses arguments.
     * Implements arguments -> (parameter_list) | lambda
     */
    public void arguments() {
        System.out.println("in arguments()");
        if (currentToken == CompToken.LEFT_PARENTHESES) {
            match(CompToken.LEFT_PARENTHESES);
            parameter_list();
            match(CompToken.RIGHT_PARENTHESES);
        } 
        // else lambda        
    } // end arguments()
    
    
    /**
     * Parses a parameter list.
     * Implements parameter_list -> identifier_list : type | identifier_list : type ; parameter_list
     */
    public void parameter_list() {
        System.out.println("in parameter_list()");
        
        ArrayList<String> identifiers = new ArrayList();
        identifiers = identifier_list(identifiers, 0); // Pass in the ArrayList and the index where we start inserting identifier names
        identifier_list(identifiers, 0);
        match(CompToken.COLON);
        type();
         if (currentToken == CompToken.SEMICOLON){
             match(CompToken.SEMICOLON);
             parameter_list();        
         }
        
    } // end parameter_list()
    
    
    /**
     * Parses optional statements
     * Implements optional_statements -> statement_list | lambda
     * @return csn: Returns the CompoundStatementNode with the statement list in it.
     */
    public CompoundStatementNode optional_statements() {
        System.out.println("in optional_statements()");
        CompoundStatementNode csn = null;
        
        if(currentToken == CompToken.END)
        {}// do nothing; leave optional_statements()
        
        else {
            ArrayList<StatementNode> statements = new ArrayList();
            statements = statement_list(statements);
            csn = new CompoundStatementNode();
            csn.setStatements(statements);
        }
        return csn;
    } // end optional_statements()
    
    
    /**
     * Parses a statement list.
     * Implements statement_list -> statement | statement ; statement_list 
     * @return statements: Returns an ArrayList of StatementNodes containing the statements.
     */
    public ArrayList<StatementNode> statement_list(ArrayList<StatementNode> statements) {
        System.out.println("in statement_list()");
        StatementNode sn = new StatementNode();
        sn = statement();
        if (sn != null) {
            statements.add(sn);
        }
        if (currentToken == CompToken.SEMICOLON) {
            match(CompToken.SEMICOLON);
            statement_list(statements);
        }
        
        
        return statements;
    } // end statement_list()
    
    
    /**
     * Parses a statement.
     * Implements statement -> variable assignop expression |
     *                          procedure_statement |
     *                          compound_statement |
     *                          if expression then statement else statement |
     *                          while expression do statement |
     *                          read(id) |
     *                          write(expression)
     * @return sn: Returns a StatementNode to statement_list() which contains the parsed statement
     */
    public StatementNode statement() {
        System.out.println("in statement()");
        System.out.println(scan.getAttribute());
        StatementNode sn = null;
        if (currentToken == CompToken.IDENTIFIER) {
            /*If the token is an identifier, it needs to be determined if it is
             a variable or a procedure statement. The symbol table will have the identifier
             kind stored. So we will send the name of the identifier to the symbol table,
             get the symbol object associated with the name key, and then get the kind
             of identifier from the symbol object.
             */

            try {
                String str = scan.getAttribute().toString(); // Get the name of the identifier
                Symbol sym = symTab.get(str); // Get the symbol value that is matched to the identifier name
                str = sym.getKind(); // Get the kind of identifier from the Symbol object

                if (str == "variable") { // If the kind of identifier is a variable
                    VariableNode vn = variable();
                    match(CompToken.ASSIGN);
                    ExpressionNode exn = expression();
                    sn = new AssignmentStatementNode(vn, exn);
                }
                if (str == "procedure") { // If the kind of identifier is a procedure
                    procedure_statement();
                }
            } // end try
            catch (Exception e) {
                error("Variable " + scan.getAttribute().toString() + " has not been declared!");
            }
        } // end if currentToken == CompToken.IDENTIFIER
        
        /*If the token is the keyword "begin", the statement will be a compound 
        statement*/
        else if(currentToken == CompToken.BEGIN){ 
            compound_statement();
        }
        
        /*If the token is the keyword "if", the statement is a conditional 
        statement
        */
        else if(currentToken == CompToken.IF){
            match(CompToken.IF); // keyword s/b "if"
            expression();
            match(CompToken.THEN); // keyword s/b "then"
            statement();
            match(CompToken.ELSE); // keyword s/b "else"
            statement();
        }
        
        /*If the token is the keyword "read", the statement will be a read
        statement.
        */
        else if(currentToken == CompToken.READ){
            match(CompToken.READ);
            match(CompToken.LEFT_PARENTHESES);
            match(CompToken.IDENTIFIER);
            match(CompToken.RIGHT_PARENTHESES);
        }
        
        /*If the token is the keyword "write", the statement will be a write
        statement.
        */
        else if(currentToken == CompToken.WRITE){
            match(CompToken.WRITE);
            match(CompToken.LEFT_PARENTHESES);
            expression();
            match(CompToken.RIGHT_PARENTHESES);
        }
        return sn;
    } // end statement()   
       
    
    /**
     * Parses a variable. Implements variable -> id | id [expression]
     * @return vn: Returns a VariableNode containing the name of the variable to statement().
     */
    public VariableNode variable() {
        System.out.println("in variable()");
        VariableNode vn = null;
        // Search the symbol table for the variable
        Symbol sym = symTab.get(scan.getAttribute().toString()); //*******************************************************************symbol table code
        if (sym != null) { // if the variable has been found
            if (sym.getKind() != "variable"){
                error(scan.getAttribute().toString() + " is not a valid variable identifier. It is a " + sym.getKind() + " identifier");
            }
            vn = new VariableNode(scan.getAttribute().toString()); // Make a VariableNode with the name of the variable loaded
            match(CompToken.IDENTIFIER);
            if (currentToken == CompToken.LEFT_SQ_BRACE) {
                match(CompToken.LEFT_SQ_BRACE);
                expression();
                match(CompToken.RIGHT_SQ_BRACE);
            }
        }
        else{ // if the variable has not been found
            error("Variable " + scan.getAttribute().toString() + " has not been declared!");
        }
        return vn;
    } // end variable()
    
    
    /**
     * Parses a procedure statement.
     * Implements procedure_statement -> id | id (expression_list)
     */
    public void procedure_statement() {
        System.out.println("in procedure_statement()");
        match(CompToken.IDENTIFIER);
        /*Does the identifier have parameters?*/
        if(currentToken == CompToken.LEFT_PARENTHESES){
            match(CompToken.LEFT_PARENTHESES);
            expression_list();
            match(CompToken.RIGHT_PARENTHESES);
        }
    } // end procedure_statement()
    
    
    /**
     * Parses an expression list
     * Implements expression_list -> expression |expression , expression_list
     */
    public void expression_list() {
        System.out.println("in expression_list()");
        expression();
        if(currentToken == CompToken.COMMA){
            match(CompToken.COMMA);
            expression_list();
        }
    } // end expression_list()
    
    
    /**
     * Parses an expression
     * Implements expression -> simple_expression | simple_expression relop simple_expression
     */
    public ExpressionNode expression() {
        System.out.println("in expression()");
        ExpressionNode exn = null;
        exn = simple_expression();
        /*If any of the relational operators follow the simple_expression,
        then we must follow the simple_expression relop simple_expression path.
        */
        if(currentToken == CompToken.EQUALS ||
           currentToken == CompToken.NOT_EQ_TO ||
           currentToken == CompToken.LESS_THAN ||
           currentToken == CompToken.LESS_THAN_EQ_TO ||
           currentToken == CompToken.GREATER_THAN_EQ_TO ||
           currentToken == CompToken.GREATER_THAN){
            /*The particular match() statement will depend on which relational operator
            is used.
            */
            switch (currentToken){
                case EQUALS:
                    match(CompToken.EQUALS);
                    break;
                case NOT_EQ_TO:
                    match(CompToken.NOT_EQ_TO);
                    break;
                case LESS_THAN:
                    match(CompToken.LESS_THAN);
                    break;
                case LESS_THAN_EQ_TO:
                    match(CompToken.LESS_THAN_EQ_TO);
                    break;
                case GREATER_THAN_EQ_TO:
                    match(CompToken.GREATER_THAN_EQ_TO);
                    break;
                case GREATER_THAN:
                    match(CompToken.GREATER_THAN);
                    break;
            }// end switch
            simple_expression();
        }// end if(currentToken)
        return exn;
    } // end expression()
    
    
    /** Parses a simple expression.
     * Implements simple_expression -> term simple_part | sign  term simple_part
     */
    public ExpressionNode simple_expression() {
        System.out.println("in simple_expression()");
        ExpressionNode exn = null;
        /*Determine if there is a sign before the term */
        if (currentToken == CompToken.PLUS
                || currentToken == CompToken.MINUS) {
            sign();
            term();
            exn = simple_part(exn);
        } else {
            exn = term();
            //OperationNode opn;
            exn = simple_part(exn);
            /* If term_part() does not return null, then we have to create an OperationNode
             that has the ExpressionNode returned by factor() as its left child, and the
             ExpressionNode returned by term_part() as its right child.
             */
          /*  if (opn != null) {
                if (opn.getRight() == null) { // the OperationNode opn has an operation and a factor only as a left child
                    opn.setRight(opn.getLeft()); // Set the factor as the right child
                    opn.setLeft(exn); // Set the first factor as the left child
                } else {  // the OperationNode opn has an operation and both a left child and a right child
                    opn.setRight(opn); // the OperationNode opn will be moved to the right child
                    opn.setLeft(exn); // the ExpressionNode exn will be set as the left child
                }
                exn = opn;
            }*/
        }
        return exn;
    } // end simple_expression()
    
    
    /**
     * Parses a simple part.
     * Implements simple_part -> addop term simple_part | lambda
     * @param exn: This is the value received from term(), which will either be attached to the
     * left child of an OperationNode, or it will be returned as is, if no OperationNode is 
     * created.
     * @return exn: This is an ExpressionNode returned to simple_expression().
     */
    public ExpressionNode simple_part(ExpressionNode exn) {
        System.out.println("in simple_part()");
        OperationNode opn = null;
        /*If there is a +, -, or "or," then we do addop term simple_part*/
        if(currentToken == CompToken.PLUS ||
           currentToken == CompToken.MINUS ||
           currentToken == CompToken.OR){
            
            opn = new OperationNode(currentToken);
            
            // We need to get the next token to send to term.
            switch(currentToken){
                case PLUS:
                    match(CompToken.PLUS);
                    break;
                case MINUS:
                    match(CompToken.MINUS);
                    break;
                case OR:
                    match(CompToken.OR);
                    break;
            }
            opn.setLeft(exn);// set the left child to the value passed in
            exn = term(); // get the next value: number or identifier
            opn.setRight(exn); // set the left child to a value or a new OperationNode
            exn = opn;
            exn = simple_part(exn); // will either be an OperationNode or a value
        }
        // else lambda
        return exn;
    } // end simple_part()
    
    
    /**
     * Parses a term.
     * Implements term -> factor term_part
     * @return exn: Returns an ExpressionNode with either a factor and an OperationNode, 
     * or a factor and a factor, or simply a factor.
     */
    public ExpressionNode term() {
        System.out.println("in term()");
        ExpressionNode exn;
        exn = factor();
        //OperationNode opn;
        exn = term_part(exn);

        /* If term_part() does not return null, then we have to create an OperationNode
         that has the ExpressionNode returned by factor() as its left child, and the
         ExpressionNode returned by term_part() as its right child.
         */
        /*
        if (opn != null) {
            if (opn.getRight() == null) { // the OperationNode opn has an operation and a factor only as a left child
                opn.setRight(opn.getLeft()); // Set the factor as the right child
                opn.setLeft(exn); // Set the first factor as the left child
            } else {  // the OperationNode opn has an operation and both a left child and a right child
                opn.setRight(opn); // the OperationNode opn will be moved to the right child
                opn.setLeft(exn); // the ExpressionNode exn will be set as the left child
            }
            exn = opn;
        }*/
        return exn;
    } // end term()
    
    
    /** 
     * Parses a term part.
     * Implements term_part -> mulop factor term_part | lambda
     * @param exn: This is the value received from term(), which will either be attached to the
     * left child of an OperationNode, or it will be returned as is, if no OperationNode is 
     * created.
     * @return exn: This is an ExpressionNode returned to term().
     */
    public ExpressionNode term_part(ExpressionNode exn) {
        System.out.println("in term_part()");
        OperationNode opn = null;  
        /*If the current token is *, /, div, mod, or and, follow mulop factor
        term_part.
        */
        if(currentToken == CompToken.MULTIPLY ||
           currentToken == CompToken.DIVIDE ||
           currentToken == CompToken.DIV ||
           currentToken == CompToken.MOD ||
           currentToken == CompToken.AND){
            
            opn = new OperationNode(currentToken);
            
            // We need to get the next token to send to factor.
            switch(currentToken){
                case MULTIPLY:
                    match(CompToken.MULTIPLY);
                    break;
                case DIVIDE:
                    match(CompToken.DIVIDE);
                    break;
                case DIV:
                    match(CompToken.DIV);
                    break;
                case MOD:
                    match(CompToken.MOD);
                    break;
                case AND:
                    match(CompToken.AND);
                    break;
            }
            
            opn.setLeft(exn);// set the left child to the value passed in
            exn = factor(); // get the next value: number or identifier
            opn.setRight(exn); // set the right child to a value or a new OperationNode
            exn = opn;
            exn = term_part(exn); // will either be an OperationNode or a value
        }
        // else lambda
        return exn;
    } // end term_part()
    
    
    /**
     * Parses a factor
     * IMplements factor -> id | id [expression] | id (expression_list | num | 
     *                      (expression) | not factor
     * @return exn: Returns an ExpressionNode that will be either a ValueNode, if it is a
     * number, or a VariableNode, if it contains an identifier.
     */
    public ExpressionNode factor() {
        System.out.println("in factor()");
        ExpressionNode exn = null;
        switch (currentToken){
            case NUMBER:
                exn = new ValueNode(scan.getAttribute().toString()); // Return a ValueNode with the number string in it.
                match(CompToken.NUMBER);
                break;
            case LEFT_PARENTHESES:
                match(CompToken.LEFT_PARENTHESES);
                expression();
                match(CompToken.RIGHT_PARENTHESES);
                break;
            case NOT:
                match(CompToken.NOT);
                factor();
                break;
            case IDENTIFIER:
                exn = new VariableNode(scan.getAttribute().toString()); // Return a VariableNode with the identifier string in it.
                match(CompToken.IDENTIFIER);
                if(currentToken == CompToken.LEFT_SQ_BRACE){
                    match(CompToken.LEFT_SQ_BRACE);
                    expression();
                    match(CompToken.RIGHT_SQ_BRACE);                    
                }
                else if(currentToken == CompToken.LEFT_PARENTHESES){
                    match(CompToken.LEFT_PARENTHESES);
                    expression_list();
                    match(CompToken.RIGHT_PARENTHESES);
                    break;                    
                }
                else { // else it is an identifier only, and we don't want to go to the default statement
                    break;
                }
            default:
                error("Illegal factor part.");
        } // end switch(currentToken)
        return exn;
    } // end factor()
    
    
    /** Parses a sign.
     * Implements sign -> + | -
     */
    public void sign() {
        System.out.println("in sign()");
        if(currentToken == CompToken.PLUS)
            match(CompToken.PLUS);
        else if(currentToken == CompToken.MINUS)
            match(CompToken.MINUS);
    } // end sign()
    
    
    
    /**
     * Returns an error message
     * @param ExpectedToken holds the CompToken expected to be scanned from the input file.
     */
    public void error(CompToken expectedToken) {
        System.out.println("Parsing error: " + currentToken);
        System.out.println("The expected token was " + expectedToken + ". The token scanned was " + currentToken + " in line " + scan.getLineCount() + " of the program.");
        System.exit(1);
    } // end error()
    
    
    /**
     * Returns a specific error message.
     * @param String Holds a specific error message for the user.
     */
    public void error(String st) {
        System.out.println("Parsing error: " + currentToken);
        System.out.println(st);
        System.exit(1);
    } // end error()
    
    
    /**
     * Getter
     * @return symTab: Returns the symbol table in use by the parser
     */
    public SymbolTable getSymbolTable(){
        return this.symTab;
    }
    
    
    /**
     * Prints the symbol table
     */
    public void printSymbolTable(){
        symTab.printTable();
    }
    
} // end CompParser
