
package CompParser;

/**
 * Enum CompToken contains all the tokens that will be encountered in the Pascal
 * program. CompToken is used to identify and validate all information scanned 
 * from the incoming program.
 * 
 * @author Van Polowchak
 */
public enum CompToken {
    // Symbols
    COLON, SEMICOLON, LEFT_PARENTHESES, RIGHT_PARENTHESES, LEFT_SQ_BRACE, RIGHT_SQ_BRACE,
    EQUALS, LESS_THAN, GREATER_THAN, NOT_EQ_TO, LESS_THAN_EQ_TO, GREATER_THAN_EQ_TO,
    PLUS, MINUS, MULTIPLY, DIVIDE, ASSIGN, PERIOD, COMMA,
    
    // Identifier
    IDENTIFIER, 
    
    // Number
    NUMBER,
    
    //Keywords
    BEGIN, END, IF, THEN, ELSE, WHILE, DO, NOT, OR, FUNCTION, PROCEDURE, DIV,
    MOD, AND, INTEGER, REAL, READ, WRITE, PROGRAM, VAR, OF, ARRAY
}
