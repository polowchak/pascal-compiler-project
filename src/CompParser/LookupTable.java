
package CompParser;

import CompParser.CompToken;
import java.util.Hashtable;

/**
 * LookupTable is a hashtable that stores the symbols and keywords used by the parser 
 * to validate input tokens.
 * @author Van Polowchak
 */
public class LookupTable extends Hashtable<String, CompToken>  {
    
    public LookupTable(){
        super();
        
        // Symbols
        this.put(":", CompToken.COLON);
        this.put(";", CompToken.SEMICOLON);
        this.put("(", CompToken.LEFT_PARENTHESES);
        this.put(")", CompToken.RIGHT_PARENTHESES);
        this.put("[", CompToken.LEFT_SQ_BRACE);
        this.put("]", CompToken.RIGHT_SQ_BRACE);
        this.put("=", CompToken.EQUALS);
        this.put("<", CompToken.LESS_THAN);
        this.put(">", CompToken.GREATER_THAN);
        this.put("<>", CompToken.NOT_EQ_TO);
        this.put("<=", CompToken.LESS_THAN_EQ_TO);
        this.put(">=", CompToken.GREATER_THAN_EQ_TO);
        this.put("+", CompToken.PLUS);
        this.put("-", CompToken.MINUS);
        this.put("*", CompToken.MULTIPLY);
        this.put("/", CompToken.DIVIDE);
        this.put(":=", CompToken.ASSIGN);
        this.put(".", CompToken.PERIOD);
        this.put(",", CompToken.COMMA);
        
        // Keywords
        this.put("array", CompToken.ARRAY);
        this.put("div", CompToken.DIV); //
        this.put("begin", CompToken.BEGIN);
        this.put("end", CompToken.END);
        this.put("if", CompToken.IF);
        this.put("then", CompToken.THEN);
        this.put("else", CompToken.ELSE);
        this.put("while", CompToken.WHILE);
        this.put("do", CompToken.DO);
        this.put("not", CompToken.NOT);
        this.put("or", CompToken.OR);
        this.put("function", CompToken.FUNCTION);
        this.put("procedure", CompToken.PROCEDURE);
        this.put("mod", CompToken.MOD);
        this.put("and", CompToken.AND);
        this.put("integer", CompToken.INTEGER);
        this.put("real", CompToken.REAL);
        this.put("read", CompToken.READ);
        this.put("write", CompToken.WRITE);
        this.put("program", CompToken.PROGRAM);
        this.put("var", CompToken.VAR);
        this.put("of", CompToken.OF);
        
    }
           
}
