package CompScanner;

/**
 * The CompScanner reads an input file and breaks it up into usable valid tokens that can
 * then be handed to the parser to evaluate. If a token is invalid, it will be returned as 
 * "TOKEN NOT AVAILABLE." If it is valid it will be returned as "TOKEN AVAILABLE," and parsing
 * will continue.
 * @author Van
 */
import CompParser.LookupTable;
import CompParser.CompToken;
import PascalCompilerProject.KeywordList;
import PascalCompilerProject.NextTokenStatus;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.util.Hashtable;



public class CompScanner {
    /**
     * State tables operate off of integer values and dictate when the scanner recognizes
     * a valid token and sends that token to the parser.
     */
    private static final int START_STATE = 0;
    private static final int IN_SYMBOL_STATE = 1;
    private static final int IN_COMMENT_STATE = 2; // old value = 4
    private static final int IN_IDENTIFIER_STATE = 3; // old value = 5
    private static final int IN_NUMBER_STATE = 4; // old value = 6
    private static final int IN_FLOAT_STATE = 5; // old value = 7
    private static final int IN_EXPONENT_STATE = 6; // old value = 8
    private static final int ERROR_STATE = 50;
    private static final int COMMENT_COMPLETE_STATE = 51;
    private static final int SYMBOL_COMPLETE_STATE = 52;
    private static final int IDENTIFIER_COMPLETE_STATE = 53;
    private static final int NUMBER_COMPLETE_STATE = 54;
    
    private int lineCount = 1; // counts the lines of the input file
    public static KeywordList numberType;  // tracks whether the current number is an integer or a real                                      
    
    private int[][] transitionTable; // table [state][char]
    private CompToken token;
    private Object attribute;
    private PushbackReader inputReader;
    private LookupTable lookupTable;
   
    /**
     * Constructor: CompScanner object
     * @param inputFile This is the file that is being compiled.
     * @param symbolTable This is a LookupTable which contains the symbols and keywords which are used by the compiler.
     */
    public CompScanner(File inputFile, LookupTable symbolTable) {
        this.lookupTable = symbolTable;

        try {
            this.inputReader = new PushbackReader(new FileReader(inputFile));
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        this.transitionTable = createTransitionTable();

    } // end CompScanner()

    
    /**
     * Getter
     * @return Returns the attribute of the scanned token
     */
    public Object getAttribute() {

        return (this.attribute);
    } // end getAttribute()

    
    /**
     * Getter
     * @return Returns the enum CompToken value 
     */
    public CompToken getToken() { 

        return (this.token);
    } // end getToken()
    
    /**
     * Getter
     * @return Returns the line count of the error in the input file
     */
    public int getLineCount(){
        return lineCount;
    }
    
    
    /**
     * Pulls the next character from the input filestream. Used for peek ahead
     * operations.
     * @return ch, the next character in the input file.
     */
    public char getNextChar(){
        char ch = 0;
        try{
            ch = (char)this.inputReader.read();
        }
        catch (IOException ioe){
            System.out.println("Read error: getNextChar()");
            System.exit(1);
            
        }
      return ch;
      
    }// end getNextChar()
    
    
    /**
     * Puts the last character pulled from the input filestream back into the
     * filestream.
     */
    public void putBack(char ch){
        try {
            inputReader.unread(ch);
        } catch (IOException ioe) {
            System.out.println("UnRead error: in putBack()");
        }        
    }// end putBack()

    
    /**
     * The workhorse of the CompScanner class. This method will break the incoming file
     * into individual usable tokens and will determine if the next token is valid. If it is
     * not valid, the scanning will be halted, and an error message that states the incorrect symbol,
     * as well as the line number will be displayed.
     * @return Returns the next token status as available, not available(illegal), or input complete.
     */
    public NextTokenStatus nextToken() {
        int currentState = START_STATE;
        StringBuilder lexeme = new StringBuilder();

        while (currentState < ERROR_STATE) {
            char currentChar = '\0';
            try {
                currentChar = (char) inputReader.read();
            } catch (Exception e) {
                System.out.println("End of input?");
                return (NextTokenStatus.INPUT_COMPLETE);
            }
            
            if (currentChar == 65535) {
                return (NextTokenStatus.INPUT_COMPLETE); // end of file
            }
            if (currentChar > 127) {
                return (NextTokenStatus.TOKEN_NOT_AVAILABLE); // illegal character
            }
            if (currentChar == '\n') {
                lineCount++;
            }
            int nextState = transitionTable[currentState][currentChar];            

            switch (nextState) {

                case START_STATE:
                    lexeme = new StringBuilder("");
                    break;
                case IN_SYMBOL_STATE:
                    lexeme.append(currentChar);
                    break;
                case IN_COMMENT_STATE:
                    while (currentChar != '}') {
                        try {
                            currentChar = (char) inputReader.read();
                            if (currentChar == '{') {
                                System.out.println("Incorrect comment format: {{ in line " + lineCount + ".\n");
                                attribute = currentChar;
                                return (NextTokenStatus.TOKEN_NOT_AVAILABLE);                                                             
                            }
                            if (currentChar == 65535) {
                                return (NextTokenStatus.INPUT_COMPLETE);
                            }
                        } catch (Exception e) {
                            System.out.println("End of input?");
                            return (NextTokenStatus.INPUT_COMPLETE);
                        }
                    }// end while
                    try {
                        inputReader.unread(currentChar);
                    } catch (IOException ioe) {
                        System.out.println("Read error: IN_COMMENT_STATE");
                    }
                    break;                
                case IN_NUMBER_STATE:
                    numberType = KeywordList.INTEGER;
                case IN_IDENTIFIER_STATE:
                case IN_EXPONENT_STATE:
                    lexeme.append(currentChar);
                    break;
                case IN_FLOAT_STATE: 
                    numberType = KeywordList.REAL;
                    lexeme.append(currentChar);
                    break;
                case ERROR_STATE:                   
                    lexeme.append(currentChar);
                    attribute = lexeme.toString();                    
                    return (NextTokenStatus.TOKEN_NOT_AVAILABLE);
                case COMMENT_COMPLETE_STATE:
                    nextState = START_STATE;   // do nothing, just eat the '}'    
                    break;
                case SYMBOL_COMPLETE_STATE:
                    boolean pushBack = true;
                    switch (currentChar) {
                        case '>':
                        case '=':
                        case '+':
                        case '-':
                        case '(':
                        case ')':
                        case '[':
                        case ']':
                        case '/':
                        case '*':
                        case ';':
                        case '.':
                        case ',':
                            lexeme.append(currentChar);
                            pushBack = false;
                    }
                    if (pushBack) {
                        try {
                            inputReader.unread(currentChar);
                        } catch (IOException ioe) {
                            System.out.println("Unread error");
                        }
                    }
                   
                    token = (CompToken) lookupTable.get(lexeme.toString());
                    attribute = lexeme.toString();
                    break;
                case IDENTIFIER_COMPLETE_STATE:
                    try {
                            inputReader.unread(currentChar);
                        } catch (IOException ioe) {
                            System.out.println("Unread error");
                        }
                    String s = lexeme.toString();
                    s = s.toLowerCase();
                    CompToken token1 = lookupTable.get(s);//******************************************* Worked w/Eric
                    if (token1 == null){  //***************************************************** Worked w/Eric
                        token = CompToken.IDENTIFIER; //****************************************** Worked w/Eric
                    }
                    else token = token1; 
                    
                        
                    /*for (KeywordList word : KeywordList.values()) {
                        if (s.equalsIgnoreCase(word.toString())) {
                            token = CompToken.KEYWORD;
                            break;
                        } else {
                            token = CompToken.IDENTIFIER;
                        }
                    }*/
                    attribute = s;
                    break;
                case NUMBER_COMPLETE_STATE:
                    try {
                        inputReader.unread(currentChar);
                    } catch (IOException ioe) {
                        System.out.println("Read error");
                    }

                    token = CompToken.NUMBER;
                    attribute = lexeme.toString();
                    break;
            } // end switch
            currentState = nextState;
        } // end while
        return (NextTokenStatus.TOKEN_AVAILABLE);
    } // end nextToken()
    
    /**
     * This method determines the state transitions of the scanner. It dictates whether the scanner
     * is in the middle of a token, or whether the token is complete, or whether there is an error with
     * an invalid character or token.
     * @return Returns an array with the current state.
     */
    private int[][] createTransitionTable() {
        return new int[][]{
            // Start state: 0
            {
                50, 50, 50, 50, 50, 50, 50, 50, 50,  0,  0, 50, 50,  0, 50, 50, // 0 - 15                    
                50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, // 16 - 31
                 0, 50, 50, 50, 50, 50, 50, 50, 52, 52, 52, 52, 52, 52, 52, 52, // 32 - 47
                 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  1, 52,  1, 52,  1, 50, // 48 - 63
                50,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3, // 64 - 79
                 3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3, 52, 50, 52, 50, 50, // 80 - 95
                50,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3, // 96 - 111
                 3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  2, 50, 50, 50, 50  // 112 - 127
            },
            // IN_SYMBOL_STATE: 1  
            {
                50, 50, 50, 50, 50, 50, 50, 50, 50, 52, 52, 50, 50, 52, 50, 50, // 0 - 15                    
                50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, // 16 - 31
                52, 50, 50, 50, 50, 50, 50, 50, 52, 52, 52, 52, 50, 52, 52, 52, // 32 - 47
                52, 52, 52, 52, 52, 52, 52, 52, 52, 52,  1, 52,  1, 52,  1, 50, // 48 - 63 
                50, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, // 64 - 79
                52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 50, 52, 50, 50, // 80 - 95
                50, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, // 96 - 111
                52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 52, 50, 52, 50, 50  // 112 - 127
            },
            // IN_COMMENT_STATE: 2
            {
                 2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2, // 0 - 15                    
                 2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2, // 16 - 31
                 2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2, // 32 - 47
                 2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2, // 48 - 63
                 2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2, // 64 - 79
                 2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2, // 80 - 95
                 2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2, // 96 - 111
                 2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2, 50,  2, 51,  2,  2  // 112 - 127
            },
            // IN_IDENTIFIER_STATE: 3
            {
                50, 50, 50, 50, 50, 50, 50, 50, 50, 53, 53, 50, 50, 53, 50, 50, // 0 - 15                    
                50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, // 16 - 31
                53, 50, 50, 50, 50, 50, 50, 50, 53, 53, 53, 53, 53, 53, 53, 53, // 32 - 47
                 3,  3,  3,  3,  3,  3,  3,  3,  3,  3, 53, 53, 53, 53, 32, 50, // 48 - 63
                50,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3, // 64 - 79
                 3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3, 53, 50, 53, 50,  3, // 80 - 95
                50,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3, // 96 - 111
                 3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3, 53, 50, 50, 50, 50  // 112 - 127               
            },
            // IN_NUMBER_STATE: 4
            {
                50, 50, 50, 50, 50, 50, 50, 50, 50, 54, 54, 50, 50, 54, 50, 50, // 0 - 15                    
                50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, // 16 - 31
                54, 50, 50, 50, 50, 50, 50, 50, 54, 54, 54, 54, 50, 54,  5, 54, // 32 - 47
                 4,  4,  4,  4 , 4,  4,  4,  4,  4,  4, 54, 54, 54, 50, 54, 50, // 48 - 63
                50, 50, 50, 50, 50,  6, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, // 64 - 79
                50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 54, 50, 54, 50, 50, // 80 - 95
                50, 50, 50, 50, 50,  6, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, // 96 - 111
                50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 54, 50, 50, 50, 50  // 112 - 127 
            },
            // IN_FLOAT_STATE: 5
            {
                54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, // 0 - 15                    
                54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, // 16 - 31
                54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 50, 54, // 32 - 47
                 5,  5,  5,  5,  5,  5,  5,  5,  5,  5, 54, 54, 54, 54, 54, 54, // 48 - 63
                54, 54, 54, 54, 54,  6, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, // 64 - 79     
                54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, // 80 - 95
                54, 54, 54, 54, 54,  6, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, // 96 - 111    
                54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54  // 112 - 127 
            },
            // IN_EXPONENT_STATE: 6
            {
                54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, // 0 - 15                    
                54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, // 16 - 31
                54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54,  6, 54,  6, 54, 54, // 32 - 47
                 6,  6,  6,  6,  6,  6,  6,  6,  6,  6, 54, 54, 54, 54, 54, 54, // 48 - 63
                54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, // 64 - 79
                54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, // 80 - 95
                54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, // 96 - 111
                54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54, 54  // 112 - 127 
            }
        }; // end return new int[][]
    } // end createTransitionTable()

} // end class CompScanner
