
package pascalcompilerproject;

/**
 * Class Symbol creates a Symbol object with two attributes: 'kind' and 'type.' The Symbol object
 * corresponds to a given identifier key value in the hashtable and describes the kind and type of the 
 * identifier.
 * 'Kind' is a string that designates the kind of the identifier: program, variable, array, or function.
 * 'Type' is a string that, if the identifier is a variable, designates the type of the identifier: integer or real.
 * Else 'Type' will be null.
 * @author Van Polowchak
 */
public class Symbol {
    
    String kind; // will be: program, variable, array, or function
    String type; // if it is a variable, type will be integer or real; otherwise it will be null
    
    /**
     * Constructor: creates a Symbol object for use in the symbol table
     * @param k This is the kind of the identifier: program, variable, array, or function
     * @param t If it is a variable, the type of the identifier will be integer or real, or else null
     */
    public Symbol(String k, String t){
        kind = k;
        type = t;
    }
    
    /**
     * Getter
     * @return Returns the kind of identifier described by the symbol: program, variable, array, or function
     */
    public String getKind(){
        return kind;
    }
    
    /**
     * Method printSymbol returns a string representation of the kind and type elements of the Symbol
     * @return Returns the string containing the kind and type.
     */
    public String printSymbol(){
        String str = "Kind: " + kind + "; Type: " + type + ".";
        return str;
    }
}// end Symbol()
