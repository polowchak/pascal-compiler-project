
package pascalcompilerproject;

/**
 * The CompParser receives a valid token from the scanner, and then it processes the token
 * according to the grammar rules. The parser assures that the input file program will
 * follow the rules of the Pascal grammar that for which it was constructed.
 * @author Van Polowchak
 */

import java.io.File;
import java.util.Enumeration;



public class CompParser {
    
    private CompToken currentToken;
    private CompScanner scan;
    //private Env top = null; // holds addresses for symbol table levels
    private int numOfIds = 100; // Number of identifiers that can be declared in one statement
    private String[] identifiers = new String[numOfIds];  // will hold multiple identifiers of the same type, if present
    private SymbolTable symTab = new SymbolTable(); // will hold the program identifiers
    
    
    public CompParser(String filename) {
        File input = new File(filename);
        LookupTable symbols = new LookupTable();
        scan = new CompScanner(input, symbols);

        // Load in the first token as the lookahead token:
        scan.nextToken();
        currentToken = scan.getToken();
        for (int i = 0; i < numOfIds; i++)
            identifiers[i] = null;   // initialize the identifiers array for use later in the parser
    } // end CompParser()
    
    
    /**
     * Parses a program.
     * Implements program --> program id; 
     *                        declarations    
     *                        subprogram_declarations  
     *                        compound_statement
     *                        .
     */
    public void program() {        
        try {
            System.out.println("in program()"); //**********************************************************************************************
            match(CompToken.PROGRAM);
            // match(CompToken.PROGRAM);
            Symbol s = new Symbol("program", "");
            symTab.put(scan.getAttribute().toString(), s); // program identifier entered into symbol table
            match(CompToken.IDENTIFIER);
            match(CompToken.SEMICOLON);
            declarations();
            subprogram_declarations();
            compound_statement();
            match(CompToken.PERIOD);
        }
        catch (Exception e) {
            error("Sorry, the program was unable to parse. The program() method failed.");                
        }
    } // end program()
    
    
    /**
     * Matches a token.
     * Matches the given token against the current token. Loads the next
     * token from the input file into the current token.
     * @param expectedToken The token to match. This parameter is a CompToken.
     */
    public void match(CompToken expectedToken) {
        System.out.println("in match()"); //***************************************************************************
              
        if(currentToken == expectedToken){
            NextTokenStatus tokenStatus = scan.nextToken();
            switch (tokenStatus) {
                case TOKEN_AVAILABLE:
                    currentToken = scan.getToken();
                    System.out.println("currentToken = " + currentToken); //*********************************************
                    break;
                case TOKEN_NOT_AVAILABLE: 
                    System.out.println("I'm right here - token not available");//***************************************************************
                    System.out.println("currentToken = " + currentToken.toString()); 
                    System.out.println("Current attribute = " + scan.getAttribute().toString());
                    error("Input error: Token not available.");
                    break;
                case INPUT_COMPLETE: // Add? ****************************************************************
                    break;
            }
        } else {
            error(expectedToken);  // We don't match!
        }
    } // end match(CompToken)
   
    
    
    
    /**
     * Parses declarations
     * Implements declarations -> var identifier_list : type ; declarations | lambda
     */
    public void declarations() {
        System.out.println("in declarations()"); //*************************************************************************
                 
        if ((currentToken == CompToken.VAR)){
            //System.out.println("I'm right here");//************************************************************************
            //Env saved = top; // point to previous level in the symbol table ***************************************************************old symbol table code
            //top = new Env(top);  // create a new level in the symbol table *****************************************************************old symbol table code
            match(CompToken.VAR);
            identifier_list(0);
            match(CompToken.COLON);
            type();
            match(CompToken.SEMICOLON);            
            declarations(); 
        }
        // else lambda
    } // end declarations()
     
    
    
     /**
     * Parses an identifier list.
     * Implements identifier_list -> id | id, identifier_list
     */
    public void identifier_list(int count) {
        System.out.println("in identifier_list()"); //**********************************************************************8
        
        // Check the symbol table to make sure the incoming identifier is not already in use.
        Symbol sym1;
        sym1 = symTab.get(scan.getAttribute().toString());
        
        // If sym1 is null, the identifier was not found in the symbol table, and it is safe to insert it
        if (sym1 == null) {
            // count is a pointer to current empty index of identifiers array, in case of multiple identifiers of the same type.
            identifiers[count] = scan.getAttribute().toString();
            match(CompToken.IDENTIFIER);

            if (currentToken == CompToken.COMMA) {
                match(CompToken.COMMA);
                // If multiple identifiers of the same type are present, we need to store them in consecutive array indexes.
                identifier_list(++count);
            }
        }
        
        // If sym1 is not null, then the incoming identifier is already in use and must not be used again
        else {
            error("Error in line number " + scan.getLineCount() + " of the input file. Identifier " + scan.getAttribute().toString() + " is already in use.");
        }
    } // end identifier_list()
    
    
    
     /**
     * Parses a type
     * Implements type -> standard_type | array [num:num] of standard_type
     */
    public void type() {
        System.out.println("in type()"); //******************************************************************************
        if (currentToken == CompToken.ARRAY) {
            match(CompToken.ARRAY);
            match(CompToken.LEFT_SQ_BRACE);
            match(CompToken.NUMBER);
            match(CompToken.COLON);
            match(CompToken.NUMBER);
            match(CompToken.RIGHT_SQ_BRACE);           
            match(CompToken.OF);           
            standard_type("a");
        }
       
        else {
            standard_type("v");
        }       
    } // end type()
    
    
    
    /**
     * Parses a standard type Implements standard_type -> integer | real 
     * Accepts a parameter of type String which designates whether the incoming
     * identifier(s) are arrays or variables
     */
    public void standard_type(String str) {
        System.out.println("in standard_type()");
        if (currentToken == CompToken.REAL || currentToken == CompToken.INTEGER) {
            if (str == "a") { // incoming identifier is an array
                int i = 0;
                while (identifiers[i] != null) { // The identifiers[] array may contain more than one array identifier
                    Symbol s = new Symbol("array", scan.getAttribute().toString());
                    symTab.put(identifiers[i], s); // Put the identifier, along with its kind and type, in the symbol table **************************symbol table code
                    i++;
                }
            }
            if (str == "v") { // incoming identifier is a variable
                int i = 0;
                while (identifiers[i] != null) { // The identifiers[] array may contain more than one variable identifier
                    Symbol s = new Symbol("variable", scan.getAttribute().toString());
                    symTab.put(identifiers[i], s); // Put the identifier, along with its kind and type, in the symbol table ****************************symbol table code
                    i++;
                }
            }
            
            if (currentToken == CompToken.INTEGER){
                match(CompToken.INTEGER);
            }
            else {  // currentToken == REAL
                match(CompToken.REAL);
            }
            
            // Reset the identifiers[] array
            for (int i = 0; i < numOfIds; i++) {
                identifiers[i] = null;
            }
           
        } else {
            error("Error in standard_type(). Must be an integer or a real.");
        }
    } // end standard_type()
   
    
    
   /**
     * Parses subprogram declarations
     * Implements subprogram_declarations -> subprogram_declaration; subprogram_declarations | lambda
     */
    public void subprogram_declarations() {
        System.out.println("in subprogram_declarations"); //*************************************************************
        
        /* If the current Token is the keyword "function" or "procedure," we'll proceed with the method.
        Otherwise, there are no subprogram_declarations.
        */       
        if (currentToken == CompToken.FUNCTION ||
                currentToken == CompToken.PROCEDURE) {
            subprogram_declaration();
            match(CompToken.SEMICOLON);
            subprogram_declarations();
        }
    } // end subprogram_declarations()
    
    
    
     /**
     * Parses a subprogram declaration
     * <code>Implements subprogram_declaration -> subprogram_head
     *                                            declarations
     *                                            subprogram_declarations
     *                                            compound_statement
     * </code>
     */
    public void subprogram_declaration() {
        System.out.println("in subprogram_declaration()");
        subprogram_head();
        declarations();
        subprogram_declarations();
        compound_statement();
    } // end subprogram_declaration()
    
    
    
    
    /**
     * Parses a subprogram head.
     * Implements subprogram_head -> function id arguments : standard_type ; | procedure id arguments ;
     */
    public void subprogram_head() {
        System.out.println("in subprogram_head()");
        
        if (currentToken == CompToken.FUNCTION){       
            match(CompToken.FUNCTION);
            Symbol s = new Symbol("function", "");
            symTab.put(scan.getAttribute().toString(), s); // Put the identifier, along with its kind (function) and type (null), in the symbol table
            match(CompToken.IDENTIFIER);
            arguments();
            match(CompToken.COLON);
            standard_type("f"); // Send an "f" to satisfy the standard_type parameter requirement, but won't be used. Stands for "function."
            match(CompToken.SEMICOLON);
        }
        else if (currentToken == CompToken.PROCEDURE){       
            match(CompToken.PROCEDURE);
            Symbol s = new Symbol("procedure", "");
            symTab.put(scan.getAttribute().toString(), s); // Put the identifier, along with its kind (procedure) and type (null), in the symbol table
            match(CompToken.IDENTIFIER);
            arguments();
            match(CompToken.SEMICOLON);
        }
    } // end subprogram_head()
    
    /**
     * Parses a compound statement
     * Implements compound_statement -> begin optional_statements end
     */
    public void compound_statement() {
        System.out.println("in compound_statement"); //********************************************************************
        match(CompToken.BEGIN);
        
        // optional_statements -> statement_list -> statement -> variable -> identifier
        if (currentToken == CompToken.IDENTIFIER)
            optional_statements();
        
        match(CompToken.END);
    } // end compound_statement()
    
    
   
   
    /**
     * Parses arguments.
     * Implements arguments -> (parameter_list) | lambda
     */
    public void arguments() {
        System.out.println("in arguments()");
        if (currentToken == CompToken.LEFT_PARENTHESES) {
            match(CompToken.LEFT_PARENTHESES);
            parameter_list();
            match(CompToken.RIGHT_PARENTHESES);
        } 
        // else lambda        
    } // end arguments()
    
    
    /**
     * Parses a parameter list.
     * Implements parameter_list -> identifier_list : type | identifier_list : type ; parameter_list
     */
    public void parameter_list() {
        System.out.println("in parameter_list()");
        identifier_list(0);
        match(CompToken.COLON);
        type();
         if (currentToken == CompToken.SEMICOLON){
             match(CompToken.SEMICOLON);
             parameter_list();        
         }
        
    } // end parameter_list()
    
    
    /**
     * Parses optional statements
     * Implements optional_statements -> statement_list | lambda
     */
    public void optional_statements() {
        System.out.println("in optional_statements()");
        
        if(currentToken == CompToken.END)
        {}// do nothing; leave optional_statements()
        
        else statement_list();
        
    } // end optional_statements()
    
    
    /**
     * Parses a statement list.
     * Implements statement_list -> statement | statement ; statement_list 
     */
    public void statement_list() {
        System.out.println("in statement_list()");
        statement();
        if(currentToken == CompToken.SEMICOLON){
            match(CompToken.SEMICOLON);
            statement_list();
        }
    } // end statement_list()
    
    
    /**
     * Parses a statement.
     * Implements statement -> variable assignop expression |
     *                          procedure_statement |
     *                          compound_statement |
     *                          if expression then statement else statement |
     *                          while expression do statement |
     *                          read(id) |
     *                          write(expression)
     */
    public void statement() {
        System.out.println("in statement()");
        System.out.println(scan.getAttribute());
        if(currentToken == CompToken.IDENTIFIER){ 
            /*If the token is an identifier, it needs to be determined if it is
            a variable or a procedure statement. The symbol table will have the identifier
            kind stored. So we will send the name of the identifier to the symbol table,
            get the symbol object associated with the name key, and then get the kind
            of identifier from the symbol object.
            */
            
            String str = scan.getAttribute().toString(); // Get the name of the identifier
            Symbol sym = symTab.get(str); // Get the symbol value that is matched to the identifier name
            str = sym.getKind(); // Get the kind of identifier from the Symbol object
            
            if(str == "variable"){ // If the kind of identifier is a variable
                variable();                
                match(CompToken.ASSIGN); 
                expression();
            }        
            if(str == "procedure"){ // If the kind of identifier is a procedure
                procedure_statement();
            } 
        }
        
        /*If the token is the keyword "begin", the statement will be a compound 
        statement*/
        else if(currentToken == CompToken.BEGIN){ 
            compound_statement();
        }
        
        /*If the token is the keyword "if", the statement is a conditional 
        statement
        */
        else if(currentToken == CompToken.IF){
            match(CompToken.IF); // keyword s/b "if"
            expression();
            match(CompToken.THEN); // keyword s/b "then"
            statement();
            match(CompToken.ELSE); // keyword s/b "else"
            statement();
        }
        
        /*If the token is the keyword "read", the statement will be a read
        statement.
        */
        else if(currentToken == CompToken.READ){
            match(CompToken.READ);
            match(CompToken.LEFT_PARENTHESES);
            match(CompToken.IDENTIFIER);
            match(CompToken.RIGHT_PARENTHESES);
        }
        
        /*If the token is the keyword "write", the statement will be a write
        statement.
        */
        else if(currentToken == CompToken.WRITE){
            match(CompToken.WRITE);
            match(CompToken.LEFT_PARENTHESES);
            expression();
            match(CompToken.RIGHT_PARENTHESES);
        }
    } // end statement()   
       
    
    /**
     * Parses a variable. Implements variable -> id | id [expression]
     */
    public void variable() {
        System.out.println("in variable()");
        
        // Search the symbol table for the variable
        Symbol sym = symTab.get(scan.getAttribute().toString()); //*******************************************************************symbol table code
        if (sym != null) { // if the variable has been found
            if (sym.kind != "variable"){
                error(scan.getAttribute().toString() + " is not a valid variable identifier. It is a " + sym.kind + " identifier");
            }
            match(CompToken.IDENTIFIER);
            if (currentToken == CompToken.LEFT_SQ_BRACE) {
                match(CompToken.LEFT_SQ_BRACE);
                expression();
                match(CompToken.RIGHT_SQ_BRACE);
            }
        }
        else{ // if the variable has not been found
            error("Variable " + scan.getAttribute().toString() + " has not been declared!");
        }
    } // end variable()
    
    
    /**
     * Parses a procedure statement.
     * Implements procedure_statement -> id | id (expression_list)
     */
    public void procedure_statement() {
        System.out.println("in procedure_statement()");
        match(CompToken.IDENTIFIER);
        /*Does the identifier have parameters?*/
        if(currentToken == CompToken.LEFT_PARENTHESES){
            match(CompToken.LEFT_PARENTHESES);
            expression_list();
            match(CompToken.RIGHT_PARENTHESES);
        }
    } // end procedure_statement()
    
    
    /**
     * Parses an expression list
     * Implements expression_list -> expression |expression , expression_list
     */
    public void expression_list() {
        System.out.println("in expression_list()");
        expression();
        if(currentToken == CompToken.COMMA){
            match(CompToken.COMMA);
            expression_list();
        }
    } // end expression_list()
    
    
    /**
     * Parses an expression
     * Implements expression -> simple_expression | simple_expression relop simple_expression
     */
    public void expression() {
        System.out.println("in expression()");
        simple_expression();
        /*If any of the relational operators follow the simple_expression,
        then we must follow the simple_expression relop simple_expression path.
        */
        if(currentToken == CompToken.EQUALS ||
           currentToken == CompToken.NOT_EQ_TO ||
           currentToken == CompToken.LESS_THAN ||
           currentToken == CompToken.LESS_THAN_EQ_TO ||
           currentToken == CompToken.GREATER_THAN_EQ_TO ||
           currentToken == CompToken.GREATER_THAN){
            /*The particular match() statement will depend on which relational operator
            is used.
            */
            switch (currentToken){
                case EQUALS:
                    match(CompToken.EQUALS);
                    break;
                case NOT_EQ_TO:
                    match(CompToken.NOT_EQ_TO);
                    break;
                case LESS_THAN:
                    match(CompToken.LESS_THAN);
                    break;
                case LESS_THAN_EQ_TO:
                    match(CompToken.LESS_THAN_EQ_TO);
                    break;
                case GREATER_THAN_EQ_TO:
                    match(CompToken.GREATER_THAN_EQ_TO);
                    break;
                case GREATER_THAN:
                    match(CompToken.GREATER_THAN);
                    break;
            }// end switch
            simple_expression();
        }// end if(currentToken)
    } // end expression()
    
    
    /** Parses a simple expression.
     * Implements simple_expression -> term simple_part | sign  term simple_part
     */
    public void simple_expression() {
        System.out.println("in simple_expression()");
        /*Determine if there is a sign before the term */
        if(currentToken == CompToken.PLUS ||
           currentToken == CompToken.MINUS){
            sign();
            term();
            simple_part();
        }
        else {
            term();
            simple_part();
        }        
    } // end simple_expression()
    
    
    /**
     * Parses a simple part.
     * Implements simple_part -> addop term simple_part | lambda
     */
    public void simple_part() {
        System.out.println("in simple_part()");
        /*If there is a +, -, or "or," then we do addop term simple_part*/
        if(currentToken == CompToken.PLUS ||
           currentToken == CompToken.MINUS ||
           currentToken == CompToken.OR){
            
            // We need to get the next token to send to term.
            switch(currentToken){
                case PLUS:
                    match(CompToken.PLUS);
                    break;
                case MINUS:
                    match(CompToken.MINUS);
                    break;
                case OR:
                    match(CompToken.OR);
                    break;
            }
            term();
            simple_part();
        }
        // else lambda
    } // end simple_part()
    
    
    /**
     * Parses a term.
     * IMplements term -> factor term_part
     */
    public void term() {
        System.out.println("in term()");
        factor();
        term_part();
    } // end term()
    
    
    /** 
     * Parses a term part.
     * Implements term_part -> mulop factor term_part | lambda
     */
    public void term_part() {
        System.out.println("in term_part()");
        /*If the current token is *, /, div, mod, or and, follow mulop factor
        term_part.
        */
        if(currentToken == CompToken.MULTIPLY ||
           currentToken == CompToken.DIVIDE ||
           currentToken == CompToken.DIV ||
           currentToken == CompToken.MOD ||
           currentToken == CompToken.AND){
            
            // We need to get the next token to send to factor.
            switch(currentToken){
                case MULTIPLY:
                    match(CompToken.MULTIPLY);
                    break;
                case DIVIDE:
                    match(CompToken.DIVIDE);
                    break;
                case DIV:
                    match(CompToken.DIV);
                    break;
                case MOD:
                    match(CompToken.MOD);
                    break;
                case AND:
                    match(CompToken.AND);
                    break;
            }
            
            factor();
            term_part();
        }
        // else lambda
    } // end term_part()
    
    
    /**
     * Parses a factor
     * IMplements factor -> id | id [expression] | id (expression_list | num | 
     *                      (expression) | not factor
     */
    public void factor() {
        System.out.println("in factor()");
        switch (currentToken){
            case NUMBER:
                match(CompToken.NUMBER);
                break;
            case LEFT_PARENTHESES:
                match(CompToken.LEFT_PARENTHESES);
                expression();
                match(CompToken.RIGHT_PARENTHESES);
                break;
            case NOT:
                match(CompToken.NOT);
                factor();
                break;
            case IDENTIFIER:
                match(CompToken.IDENTIFIER);
                if(currentToken == CompToken.LEFT_SQ_BRACE){
                    match(CompToken.LEFT_SQ_BRACE);
                    expression();
                    match(CompToken.RIGHT_SQ_BRACE);                    
                }
                else if(currentToken == CompToken.LEFT_PARENTHESES){
                    match(CompToken.LEFT_PARENTHESES);
                    expression_list();
                    match(CompToken.RIGHT_PARENTHESES);
                    break;                    
                }
                // else it is an identifier only.
            default:
                error("Illegal factor part.");
        } // end switch(currentToken)
    } // end factor()
    
    
    /** Parses a sign.
     * Implements sign -> + | -
     */
    public void sign() {
        System.out.println("in sign()");
        if(currentToken == CompToken.PLUS)
            match(CompToken.PLUS);
        else if(currentToken == CompToken.MINUS)
            match(CompToken.MINUS);
    } // end sign()
    
    
    
    /**
     * Returns an error message
     * @param ExpectedToken holds the CompToken expected to be scanned from the input file.
     */
    public void error(CompToken expectedToken) {
        System.out.println("Parsing error: " + currentToken);
        System.out.println("The expected token was " + expectedToken + ". The token scanned was " + currentToken + " in line " + scan.getLineCount() + " of the program.");
        System.exit(1);
    } // end error()
    
    
    /**
     * Returns a specific error message.
     * @param String Holds a specific error message for the user.
     */
    public void error(String st) {
        System.out.println("Parsing error: " + currentToken);
        System.out.println(st);
        System.exit(1);
    } // end error()
    
    
    /**
     * Prints the symbol table
     */
    public void printSymbolTable(){
        symTab.printTable();
    }
    
} // end CompParser
