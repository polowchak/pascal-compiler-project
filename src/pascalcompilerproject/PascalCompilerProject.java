
package pascalcompilerproject;

/**
 * PascalCompilerProject contains the main() function that controls the operation
 * of the compiler.
 */

import java.io.File;

public class PascalCompilerProject {

    /**
     * @param args the command line arguments. In this case the command line argument is the
     * file to be opened and parsed.
     */
    public static void main(String[] args) {
        
        String filename = args[0];        
        File input = new File(filename);
        LookupTable symbols = new LookupTable();
        CompScanner compScan = new CompScanner(input, symbols);
        CompParser parse = new CompParser(filename);
        NextTokenStatus nextToken;
        
        /*
        The following code was initially programmed to activate the scanner and includes
        output statements that shows what each token scanned successfully is. However, the 
        parser also activates the scanner, so this code is not necessary for the operation of
        the parser. Therefore, it has commented out, but I have left it in for the purpose of
        reuse, should it become necessary.
        */
       
        /*
        while ((nextToken = compScan.nextToken()) == NextTokenStatus.TOKEN_AVAILABLE) {
            System.out.println("Return value is " + nextToken);
                  
            CompToken ct = compScan.getToken();
            Object objAttr = compScan.getAttribute();
            System.out.println("Token: " + ct + "     Attribute: [" + objAttr + "]\n");            
        } // end while    
            if (nextToken == NextTokenStatus.TOKEN_NOT_AVAILABLE)
                System.out.println("The input file contains scanning errors: " + compScan.getAttribute() 
                        + " in line " + compScan.getLineCount());
            System.out.println();
            
            if (nextToken == NextTokenStatus.INPUT_COMPLETE)
                System.out.println("The input file scanned successfully!");
        */    
            parse.program();
            System.out.println("The input file parsed successfully!");
            
            parse.printSymbolTable();
        
    }// end main()
    
}// end class PascalCompilerProject
