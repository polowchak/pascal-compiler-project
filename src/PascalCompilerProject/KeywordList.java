/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PascalCompilerProject;

/**
 *
 * @author Van
 */
public enum KeywordList {
    ARRAY, BEGIN, END, IF, THEN, ELSE, WHILE, DO, NOT, OR, FUNCTION, PROCEDURE, DIV,
    MOD, AND, INTEGER, REAL, READ, WRITE, PROGRAM, VAR, OF;
    
    public static final int length = 21;
}
