
package PascalCompilerProject;

/**
 * Simple class to enumerate the three possible options for the status of the next token.
 * @author Van Polowchak
 */
public enum NextTokenStatus {
    
    TOKEN_AVAILABLE, TOKEN_NOT_AVAILABLE, INPUT_COMPLETE
}
