
package SyntaxTree;

import CompParser.CompParser;
import SymbolTable.SymbolTable;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;


/**
 *
 * @author Van Polowchak
 */
public class VariableNode extends ExpressionNode{
   
    /** Variable name */
    private String name;
    
    
    /** Constructor 
     @param name: The name of the variable
     */
    public VariableNode(String name){
        this.name = name;
    }
  
    
     @Override
    public String indentedToString( int level) {
      
        String answer = super.indentedToString(level);
        answer += "Value: " + this.name + "\n";
        return answer;
    }
    
} // end class VariableNode
