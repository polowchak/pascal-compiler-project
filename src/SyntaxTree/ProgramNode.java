
package SyntaxTree;

/**
 *
 * @author Van Polowchak
 */
public class ProgramNode extends CompilerNode {
    
    /** Name of the program */
    private String name;
    
    
    /** Declarations Node */
    private DeclarationsNode variables;
    /** Setter*/
    public void setVariables(DeclarationsNode v){
        this.variables = v;
    }
    
    
    
    /** Compound Statement Node */
    private CompoundStatementNode main;
    /** Setter */
    public void setMain(CompoundStatementNode m){
        this.main = m;
    }
    
    
    
    /** Subprogram Declarations Node */
    private SubprogramDeclarationsNode functions;
    /** Setter */
    public void setFunctions(SubprogramDeclarationsNode f){
        this.functions = f;
    }
    
    
    /** Constructor 
     @param name: The name of the program
     */
    public ProgramNode(String name){
        this.name = name;
    }
    
        
    
     @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "ProgramNode: " + this.name + "\n";
        if(variables != null){
            answer += variables.indentedToString(level + 1);
        }
         if(main != null){
            answer += main.indentedToString(level + 1);
        }
          if(functions != null){
            answer += functions.indentedToString(level + 1);
        }
        return answer;
    }
    
} // end class ProgramNode
