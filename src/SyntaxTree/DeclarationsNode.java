
package SyntaxTree;

import java.util.ArrayList;

/**
 *
 * @author Van Polowchak
 */
public class DeclarationsNode extends CompilerNode {
    
    /** ArrayList */
    private ArrayList<VariableNode> vars;
    
    
    /** Setter */
    public void addVariable(VariableNode var){
       vars.add(var);
    }
    
    
    /** Constructor */
    public DeclarationsNode(){
        this.vars = new ArrayList();
    }
    
    
    /**
     * String printout of the DeclarationsNode
     */
    @Override
    public String indentedToString(int level) {
        String answer = super.indentedToString(level);
        //answer += "DeclarationsNode: " + this.vars + "\n";
        answer += "DeclarationsNode:\n";
        if (vars != null) {
            for (int i = 0; i < vars.size(); i++) {
                VariableNode v = (vars.get(i));
                answer += v.indentedToString(level + 1);
            }
        }
        return answer;
    }
    
    
} // end class DeclarationsNode
