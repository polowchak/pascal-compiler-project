
package SyntaxTree;

import java.util.ArrayList;

/**
 *
 * @author Van Polowchak
 */
public class CompoundStatementNode extends StatementNode {

    /**
     * ArrayList variable
     */
    private ArrayList<StatementNode> statements;

    /**
     * Setter
     */
    public void setStatements(ArrayList<StatementNode> st) {
        this.statements = st;
    }
    
    /**
     * Add a statement to the ArrayList
     */
    public void addStatement(StatementNode sn){
        statements.add(sn);
    }

    /**
     * Constructor
     */
    public CompoundStatementNode() {
        this.statements = new ArrayList();
    }

    @Override
    public String indentedToString(int level) {

        String answer = super.indentedToString(level);
        answer += "CompoundStatementNode: \n";

        if (statements != null) {

            for (int i = 0; i < statements.size(); i++) {
                StatementNode exp = statements.get(i);
                answer += exp.indentedToString(level + 1);
                /**
                if (statements.get(i) instanceof AssignmentStatementNode) {
                    AssignmentStatementNode asn = (AssignmentStatementNode) (statements.get(i));
                    answer += asn.indentedToString(level + 1);
                } else { // statements.get(i) instanceof CompoundStatementNode)
                    CompoundStatementNode csn = (CompoundStatementNode) (statements.get(i));
                    answer += csn.indentedToString(level + 1);
                } */
            }
        }
        return answer;

    } // end indentedToString()

} // end class CompoundStatementNode
