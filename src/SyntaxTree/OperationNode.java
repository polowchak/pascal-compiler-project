
package SyntaxTree;

import CompParser.CompToken;
/**
 *
 * @author Van Polowchak
 */
public class OperationNode extends ExpressionNode {
     /** The left operator of this operation. */
    private ExpressionNode left = null;
    
    /** The right operator of this operation. */
    private ExpressionNode right = null;
    
    /** The kind of operation. */
    private CompToken operation;
    
    /**
     * Constructor
     * Creates an operation node given an operation token.
     * @param op The token representing this node's math operation.
     */
    public OperationNode ( CompToken op) {
        this.operation = op;
    }
    
    
    // Getters
    public ExpressionNode getLeft() { return( this.left);}
    public ExpressionNode getRight() { return( this.right);}
    public CompToken getOperation() { return( this.operation);}
    
    // Setters
    public void setLeft( ExpressionNode node) { this.left = node;}
    public void setRight( ExpressionNode node) { this.right = node;}
    public void setOperation( CompToken op) { this.operation = op;}
    
           
    /**
     * Returns the operation token as a String.
     * @return The String version of the operation token.
     */
    @Override
    public String toString() {
        return operation.toString();
    }
    
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Operation: " + this.operation + "\n";
        //answer += super.indentedToString(level + 1) + "LValue:\n";
        answer += left.indentedToString(level + 1);
        //answer += super.indentedToString(level + 1) + "RValue:\n";
        answer += right.indentedToString(level + 1);
        return( answer);
    }
} // end class OperationNode
