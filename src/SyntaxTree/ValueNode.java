
package SyntaxTree;

/**
 *
 * @author Van Polowchak
 */
public class ValueNode extends ExpressionNode{
    
    /** The attribute associated with this node. */
    String attribute;
    
    /**
     * Creates a ValueNode with the given attribute.
     * @param attr The attribute for this value node.
     */
    public ValueNode( String attr) {
        this.attribute = attr;
    }
    
    /** 
     * Returns the attribute of this node.
     * @return The attribute of this ValueNode.
     */
    public String getAttribute() { return( this.attribute);}
    
    /**
     * Returns the attribute as the description of this node.
     * @return The attribute String of this node.
     */
    @Override
    public String toString() {
        return( attribute);
    }
    
    @Override
    public String indentedToString( int level) {
        String answer = super.indentedToString(level);
        answer += "Value: " + this.attribute + "\n";
        return answer;
    }
} // end class ValueNode
