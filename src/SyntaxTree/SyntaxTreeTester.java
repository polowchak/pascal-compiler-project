
package SyntaxTree;

import CompParser.CompToken;

/**
 *
 * @author Van Polowchak
 */
public class SyntaxTreeTester {

    public static void main(String[] args) {
        
        /* Initiate the ProgramNode */
        ProgramNode pro = new ProgramNode("testNode1");
        
        
        
        
        /*****************************************************************/
        /**************  DeclarationsNode  *******************************/
        
        /* Initiate the DeclarationsNode */
        DeclarationsNode dec = new DeclarationsNode();
        
        /* Load the DeclarationsNode with variables */
        VariableNode var1 = new VariableNode("var1");
        VariableNode var2 = new VariableNode("var2");
        //VariableNode var3 = new VariableNode("var3");
        dec.addVariable(var1);
        dec.addVariable(var2);
        //dec.addVariable(var3);
        
        /* Add the DeclarationsNode to the ProgramNode */
        pro.setVariables(dec);
        
        
        
        
        /*****************************************************************/
        /************  CompoundStatementNode  ****************************/
        
        /* Initiate the CompoundStatementNode */
        CompoundStatementNode csn = new CompoundStatementNode();
        
        /* Make AssignmentStatementNodes */
        ExpressionNode exp1 = new ValueNode("6");
        ExpressionNode exp3 = new ValueNode("8");
        /* Set up OperationNode */
        OperationNode on1 = new OperationNode(CompToken.PLUS);
        on1.setLeft(var1);
        on1.setRight(exp3);
        /* Set AssignmentStatementNode */
        StatementNode sn1 = new AssignmentStatementNode(var2, on1);
        StatementNode sn2 = new AssignmentStatementNode(var1, exp1);
        
        /* Load the CompoundStatementNode */
        csn.addStatement(sn2);
        csn.addStatement(sn1);
        
        /* Add the CompoundStatementNode to the ProgramNode */
        pro.setMain(csn);
        
        
        
        /* Print the syntax tree */
        String answer = pro.indentedToString(0);
        System.out.println(answer);
        
        
    } // end main()
}// end class SyntaxTreeTester
