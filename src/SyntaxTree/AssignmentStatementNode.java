
package SyntaxTree;

/**
 *
 * @author Van Polowchak
 */
public class AssignmentStatementNode extends StatementNode{
     
    /** Variable */
    private VariableNode lvalue;
    /** Setter */
    public void setLValue(VariableNode lv){
        this.lvalue = lv;
    }
    
    /** Variable */
    private ExpressionNode expression;
    /** Setter */
    public void setExpression(ExpressionNode ex){
        this.expression = ex;
    }
    
    /** Constructor */
    public AssignmentStatementNode(VariableNode vn, ExpressionNode ex){
        this.lvalue = vn;
        this.expression = ex;
    }
    
    /** Getter */
    public VariableNode getLValue(){
        return this.lvalue;
    }
    
    /** Getter */
    public ExpressionNode getExpression(){
        return this.expression;
    }
    
    
      @Override
    public String indentedToString( int level) {
      
        String answer = super.indentedToString(level);
        answer += "AssignmentStatementNode: \n";
        //answer += super.indentedToString(level + 1);
        
        if(lvalue != null){
            answer += lvalue.indentedToString(level + 1);
        }
        
        //answer += super.indentedToString(level + 1);
        
        if(expression != null){
            answer += expression.indentedToString(level + 1);
        }
        return answer;
    }
    
    
    
    
} // end class AssignmentStatementNode
